/*
 * Copyright Perforator, Inc. and contributors. All rights reserved.
 *
 * Use of this software is governed by the Business Source License
 * included in the LICENSE file.
 *
 * As of the Change Date specified in that file, in accordance with
 * the Business Source License, use of this software will be governed
 * by the Apache License, Version 2.0.
 */
package io.perforator.sdk.maven;

class ClassNames {
    
    static final String LOAD_GENERATOR_CONFIG = "io.perforator.sdk.loadgenerator.core.configs.LoadGeneratorConfig";
    
    static final String EMBEDDED_LOAD_GENERATOR = "io.perforator.sdk.loadgenerator.embedded.EmbeddedLoadGenerator";
    static final String EMBEDDED_SUITE_CONFIG = "io.perforator.sdk.loadgenerator.embedded.EmbeddedSuiteConfig";
    
    static final String TESTNG_LOAD_GENERATOR = "io.perforator.sdk.loadgenerator.testng.TestNGLoadGenerator";
    static final String TESTNG_SUITE_CONFIG = "io.perforator.sdk.loadgenerator.testng.TestNGSuiteConfig";
    
}
