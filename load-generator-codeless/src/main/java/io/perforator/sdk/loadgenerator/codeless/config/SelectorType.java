/*
 * Copyright Perforator, Inc. and contributors. All rights reserved.
 *
 * Use of this software is governed by the Business Source License
 * included in the LICENSE file.
 *
 * As of the Change Date specified in that file, in accordance with
 * the Business Source License, use of this software will be governed
 * by the Apache License, Version 2.0.
 */
package io.perforator.sdk.loadgenerator.codeless.config;

public enum SelectorType {

    /**
     * CSS selectors are used to target the HTML elements on web pages
     */
    css,

    /**
     * XPath uses path expressions to select nodes or node-sets in an XML document.
     */
    xpath
}