/*
 * Copyright Perforator, Inc. and contributors. All rights reserved.
 *
 * Use of this software is governed by the Business Source License
 * included in the LICENSE file.
 *
 * As of the Change Date specified in that file, in accordance with
 * the Business Source License, use of this software will be governed
 * by the Apache License, Version 2.0.
 */
package io.perforator.sdk.loadgenerator.codeless.actions;

import com.fasterxml.jackson.databind.JsonNode;
import com.google.auto.service.AutoService;
import io.perforator.sdk.loadgenerator.codeless.FormattingMap;
import io.perforator.sdk.loadgenerator.codeless.config.CodelessLoadGeneratorConfig;
import io.perforator.sdk.loadgenerator.codeless.config.CodelessSuiteConfig;
import java.util.concurrent.TimeUnit;
import org.openqa.selenium.remote.RemoteWebDriver;

@SuppressWarnings("rawtypes")
@AutoService(ActionProcessor.class)
public class OpenActionProcessor extends AbstractActionProcessor<OpenActionConfig, OpenActionInstance> {

    public OpenActionProcessor() {
        super(OpenActionConfig.DEFAULT_ACTION_NAME);
    }

    @Override
    public OpenActionConfig buildActionConfig(String actionName, JsonNode actionValue) {
        return OpenActionConfig.builder()
                .url(
                        getRequiredValueOrNestedField(
                                OpenActionConfig.Fields.url,
                                actionValue
                        )
                )
                .timeout(
                        getOptionalNestedField(
                                OpenActionConfig.Fields.timeout,
                                actionValue,
                                null
                        )
                )
                .enabled(
                        getOptionalNestedField(
                                OpenActionConfig.Fields.enabled,
                                actionValue,
                                "true"
                        )
                )
                .build();
    }

    @Override
    public OpenActionInstance buildActionInstance(CodelessLoadGeneratorConfig loadGeneratorConfig, CodelessSuiteConfig suiteConfig, FormattingMap formatter, OpenActionConfig actionConfig) {
        return OpenActionInstance.builder()
                .config(
                        actionConfig
                )
                .url(
                        buildUrlForActionInstance(
                                OpenActionConfig.Fields.url,
                                actionConfig.getUrl(),
                                formatter
                        )
                )
                .timeout(
                        buildDurationForActionInstance(
                                OpenActionConfig.Fields.timeout,
                                actionConfig.getTimeout(),
                                suiteConfig.getWebDriverSessionPageLoadTimeout(),
                                formatter,
                                false
                        )
                )
                .enabled(
                        buildEnabledForActionInstance(
                                OpenActionConfig.Fields.enabled, 
                                actionConfig.getEnabled(), 
                                formatter
                        )
                )
                .build();
    }

    @Override
    public void processActionInstance(RemoteWebDriver driver, OpenActionInstance actionInstance) {
        driver.manage().timeouts().pageLoadTimeout(
                actionInstance.getTimeout()
        );

        driver.navigate().to(actionInstance.getUrl());
    }

}
