package com.example.browser_clouds;

import io.perforator.sdk.api.okhttpgson.ApiClientBuilder;
import io.perforator.sdk.api.okhttpgson.invoker.ApiException;
import io.perforator.sdk.api.okhttpgson.operations.BrowserCloudsApi;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class CheckIfBrowserCloudExistsExample {

    Logger logger = LoggerFactory.getLogger(
            CheckIfBrowserCloudExistsExample.class
    );
    
    String apiBaseUrl = ApiClientBuilder.DEFAULT_API_BASE_URL;
    
    //Please replace YOUR_CLIENT_ID with you own client id
    String apiClientId = "YOUR_CLIENT_ID";
    
    //Please replace YOUR_CLIENT_SECRET with you own client secret
    String apiClientSecret = "YOUR_CLIENT_SECRET";
    
    //Please replace YOUR_PROJECT_KEY with you own project key
    String projectKey = "YOUR_PROJECT_KEY";
    
    //Please replace YOUR_EXECUTION_KEY with you own execution key
    String executionKey = "YOUR_EXECUTION_KEY";
    
    //Please replace YOUR_BROWSER_CLOUD_KEY with you own browser cloud key
    String browserCloudKey = "YOUR_BROWSER_CLOUD_KEY";
    
    public void run() {
        ApiClientBuilder builder = new ApiClientBuilder(
                apiClientId,
                apiClientSecret,
                apiBaseUrl
        );

        BrowserCloudsApi browserCloudsApi = builder.getApi(
                BrowserCloudsApi.class
        );

        try {
            browserCloudsApi.checkIfBrowserCloudExists(
                    projectKey,
                    executionKey,
                    browserCloudKey
            );
            logger.info(
                    "Browser cloud with the key {} exists.",
                    browserCloudKey
            );
        } catch (ApiException e) {
            logger.info(
                    "Browser cloud with the key {} doesn't exist",
                    browserCloudKey
            );
        }
    }

    public static void main(String[] args) throws Exception {
        new CheckIfBrowserCloudExistsExample().run();
    }

}
