package com.example.limits;

import com.example.AbstractExampleTest;
import java.util.Collections;
import java.util.Map;

public class VerifyLimitsExampleTest extends AbstractExampleTest<VerifyLimitsExample>{

    @Override
    protected Map<String, Object> getAdditionalFields() {
        return Collections.EMPTY_MAP;
    }
    
}
