package com.example;

import java.util.Collections;
import java.util.Map;

public class ApiClientsExampleTest extends AbstractExampleTest<ApiClientsExample> {

    @Override
    protected Map<String, Object> getAdditionalFields() {
        return Collections.EMPTY_MAP;
    }
    
}
