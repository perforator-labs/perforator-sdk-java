package com.example.projects;

import java.util.Collections;
import java.util.Map;

public class ListProjectsExampleTest extends AbstractProjectExampleTest<ListProjectsExample> {

    @Override
    protected Map<String, Object> getAdditionalFields() {
        return Collections.EMPTY_MAP;
    }
    
}
